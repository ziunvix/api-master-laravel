<?php

namespace Tests\Unit;

use App\Models\Locale;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class LocaleTest extends TestCase
{
    /**
     * Check if locale list is private. only user can see his locale.
     *
     * @return void
     */
    public function test_can_not_access_private_locale_api()
    {
        $response = $this->get('/api/lang/en');

        $response->assertStatus(401);
    }

    /**
     * Test if locale is creatable.
     *
     * @return void
     */
    public function test_can_create_locale()
    {
        // Login the user first.
        Auth::login(User::where('email', 'admin@example.com')->first());

        // First count total number of locale
        $totalLocales = Locale::get('id')->count();

        $locale = Locale::create([
            'locale'       => 'en',
            'user_id'     => 1
        ]);

        $this->assertDatabaseCount('locales', $totalLocales + 1);

        // Delete the policy as need to keep it in database for other tests
        $locale = Locale::where('locale', 'en')->where('user_id', 1)->first();
        $locale->delete();
    }
}

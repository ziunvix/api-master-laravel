<?php

namespace Tests\Unit;

use App\Models\Policy;
use App\Models\User;
use App\Repositories\PolicyRepository;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class PolicyTest extends TestCase
{
    /**
     * Check if public profile api is accessible or not.
     *
     * @return void
     */
    public function test_can_access_public_policy_api()
    {
        $response = $this->get('/api/policy/view/all');

        $response->assertStatus(200);
    }

    /**
     * Check if policy list is private. only user can see his policies.
     *
     * @return void
     */
    public function test_can_not_access_private_policy_api()
    {
        $response = $this->get('/api/policy');

        $response->assertStatus(401);
    }

    /**
     * Test if policy is creatable.
     *
     * @return void
     */
    public function test_can_create_policy()
    {
        // Login the user first.
        Auth::login(User::where('email', 'admin@example.com')->first());
        $policyRepository = new PolicyRepository();

        // First count total number of policies
        $totalPolicies = Policy::get('id')->count();

        $policy = $policyRepository->create([
            'name'       => 'policy:test',
            'user_id'     => 1
        ]);

        $this->assertDatabaseCount('policies', $totalPolicies + 1);

        // Delete the policy as need to keep it in database for other tests
        $policy = Policy::where('name', 'policy:test')->where('user_id', 1)->first();
        $policy->delete();
    }
}

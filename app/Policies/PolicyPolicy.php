<?php

namespace App\Policies;

use App\Models\Policy;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;
use App\Policies\BasePolicy;

class PolicyPolicy extends BasePolicy
{
    use HandlesAuthorization;

    private array $global_access = ["policy:all"];

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Policy  $policy
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user)
    {
        return $this->check($user, collect(["policy:view"])->merge($this->global_access));
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return $this->check($user, collect(["policy:add"])->merge($this->global_access));
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Policy  $policy
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Policy $policy)
    {
        //return ($user->id === $policy->user_id) || ($this->check($user, collect(["policy:update"])->merge($this->global_access)));
        return $this->check($user, collect(["policy:update"])->merge($this->global_access));
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Policy  $policy
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Policy $policy)
    {
        return $this->check($user, collect(["policy:delete"])->merge($this->global_access));
    }

}

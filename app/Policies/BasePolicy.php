<?php

namespace App\Policies;

use App\Models\Policy;
use App\Models\User;

class BasePolicy
{
    /**
     * Determine whether the user can view any models.
     *
     */
    public function check($user , $policy_array)
    {
        $policies = collect($user->policy);

        foreach ($policy_array as $policy_name) {
            if ($policies->contains('name', $policy_name))
                return true;
        }
    }

}

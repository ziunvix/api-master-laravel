<?php

namespace App\Repositories;

use Illuminate\Support\Str;
use App\Helpers\UploadHelper;
use App\Interfaces\CrudInterface;
use App\Models\Policy;
use App\Models\User;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;

class PolicyRepository implements CrudInterface
{
    /**
     * Authenticated User Instance.
     *
     * @var User
     */
    public User | null $user;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->user = Auth::guard()->user();
    }

    /**
     * Get All Policies.
     *
     * @return collections Array of Policy Collection
     */
    public function getAll(): Paginator
    {
        return $this->user->policy()
            ->orderBy('id', 'desc')
            ->with('user')
            ->paginate(10);
    }

    /**
     * Get Paginated Policy Data.
     *
     * @param int $pageNo
     * @return collections Array of Policy Collection
     */
    public function getPaginatedData($perPage): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 12;
        return Policy::orderBy('id', 'desc')
            ->with('user')
            ->paginate($perPage);
    }

    /**
     * Get Searchable Policy Data with Pagination.
     *
     * @param int $perPage
     * @return collections Array of Policy Collection
     */
    public function searchPolicy($keyword, $perPage): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 10;

        return Policy::where('name', 'like', '%' . $keyword . '%')
            ->orderBy('id', 'desc')
            ->with('user')
            ->paginate($perPage);
    }

    /**
     * Create New Policy.
     *
     * @param array $data
     * @return object Policy Object
     */
    public function create(array $data): Policy
    {
        $data['user_id'] = $data['user_id']??  $this->user->id;
        $data['status'] = $data['status']?? 1;

        return Policy::create($data);
    }

    /**
     * Delete Policy.
     *
     * @param int $id
     * @return boolean true if deleted otherwise false
     */
    public function delete(int $id): bool
    {
        $policy = Policy::find($id)->delete();
        if (empty($policy)) {
            return false;
        }
        return true;
    }

    /**
     * Get Policy Detail By ID.
     *
     * @param int $id
     * @return void
     */
    public function getByID(int $id): Policy|null
    {
        return Policy::with('user')->find($id);
    }

    /**
     * Get Policy Detail By ID.
     *
     * @param int $id
     * @return void
     */
    public function getByNameAndUserID(int $user_id, $name): Policy|null
    {
        return Policy::where('user_id', $user_id)
            ->where('name', $name)->first();
    }


    /**
     * Update Policy By User ID & Policy Name.
     *
     * @param int $user_id
     * @param array $data
     * @return object Updated Policy Object
     */
    public function update(int $user_id, array $data)
    {
        $data['status'] = $data['status']?? 1;

        $policy = $this->getByNameAndUserID($user_id,$data['name']);

        if (is_null($policy)) {
            return null;
        }

        // If everything is OK, then update.
        $policy->update(['status' => $data['status']]);

        // Finally, return the updated Policy.
        return $this->getByID($policy->id);
    }

    /**
     * Get Policy Detail By ID.
     *
     * @param int $id
     * @return void
     */
    public function getByUserID(int $user_id)
    {
        return Policy::where('user_id', $user_id)->get();
    }

}

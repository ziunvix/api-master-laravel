<?php

namespace App\Http\Controllers;

use App\Models\Locale;
use App\Models\Policy;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\App;
use App\Http\Requests\LocaleRequest;

class LocaleController extends Controller
{
    /**
     * Response trait to handle return responses.
     */
    use ResponseTrait;
    /**
     * @OA\GET(
     *     path="/api/lang/{locale}",
     *     tags={"Locale"},
     *     summary="Change Locale",
     *     description="Change user locale language",
     *     operationId="LocaleUpdate",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="locale", description="change language of api, eg; en", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200,description="Change locale successfuly"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function set_locale(LocaleRequest $request, $locale): JsonResponse
    {

        if (!in_array($locale, ['en', 'fa'])) {
            return $this->responseError(null, 'Locale does not exist', Response::HTTP_NOT_FOUND);
        }
        Locale::updateOrCreate(
            [
                'user_id'    => Auth::id()
            ],
            [
                'user_id'  => Auth::id(),
                'locale' => $locale,
            ]
        );

        App::setLocale($locale);

        $get_current_locale = App::currentLocale();

        return $this->responseSuccess(null, 'Locale successfully changed to [' . $get_current_locale.']');
    }

}

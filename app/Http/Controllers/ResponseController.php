<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Traits\ResponseTrait;
use Symfony\Component\HttpFoundation\Response;

class ResponseController extends Controller
{
    use ResponseTrait;

    public function not_found()
    {
        return $this->responseError(null,'Requested URI does not exist', Response::HTTP_NOT_FOUND);
    }

}

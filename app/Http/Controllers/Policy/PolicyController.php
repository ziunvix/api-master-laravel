<?php

namespace App\Http\Controllers\Policy;

use App\Http\Controllers\Controller;
use App\Http\Requests\Policy\CreatePolicyRequest;
use App\Http\Requests\Policy\DestroyPolicyRequest;
use App\Http\Requests\Policy\ShowPolicyRequest;
use App\Http\Requests\Policy\UpdatePolicyRequest;
use App\Models\Policy;
use App\Repositories\PolicyRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Traits\ResponseTrait;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;


class PolicyController extends Controller
{
    /**
     * Response trait to handle return responses.
     */
    use ResponseTrait;

    /**
     * Policy Repository class.
     *
     * @var PolicyRepository
     */
    public $policyRepository;

    public function __construct(PolicyRepository $policyRepository)
    {
        $this->middleware('auth:api', ['except' => ['indexAll']]);
        $this->policyRepository = $policyRepository;
    }

    /**
     * @OA\GET(
     *     path="/api/policy",
     *     tags={"Policy"},
     *     summary="Get Policy List",
     *     description="Get Policy List as Array",
     *     operationId="PolicyIndex",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Policy List as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function index(Request $request): JsonResponse
    {
        try {
            if ($request->user()->cannot('view', Policy::class))
                return $this->responseError(null, 'Show Policy Is Forbidden', Response::HTTP_FORBIDDEN);

            $data = $this->policyRepository->getAll();
            return $this->responseSuccess($data, 'Policy List Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/policy/view/all",
     *     tags={"Policy"},
     *     summary="All Policy - Publicly Accessible",
     *     description="All Policy - Publicly Accessible",
     *     operationId="PolicyindexAll",
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="All Policy - Publicly Accessible" ),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function indexAll(Request $request): JsonResponse
    {
        try {
            $data = $this->policyRepository->getPaginatedData($request->perPage);
            return $this->responseSuccess($data, 'Policy List Fetched Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/policy/view/search",
     *     tags={"Policy"},
     *     summary="All Policy - Publicly Accessible",
     *     description="All Policy - Publicly Accessible",
     *     operationId="PolicySearch",
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="search", description="search, eg; Test", example="Test", in="query", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="All Policy - Publicly Accessible" ),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function search(Request $request): JsonResponse
    {
        try {
            $data = $this->policyRepository->searchPolicy($request->search, $request->perPage);
            return $this->responseSuccess($data, 'Policy List Fetched Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/policy",
     *     tags={"Policy"},
     *     summary="Create New Policy",
     *     description="Create New Policy",
     *     operationId="PolicyStore",
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="name", type="string", example="policy:add"),
     *              @OA\Property(property="user_id", type="integer", example=2052),
     *              @OA\Property(property="status", type="boolean", example=1)
     *          ),
     *      ),
     *      security={{"bearer":{}}},
     *      @OA\Response(response=200, description="Create New Policy" ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function store(CreatePolicyRequest $request): JsonResponse
    {
        try {
            if ($request->user()->cannot('create', Policy::class))
                return $this->responseError(null, 'Create Policy Is Forbidden', Response::HTTP_FORBIDDEN);

            $policy = $this->policyRepository->create($request->all());
            return $this->responseSuccess($policy, 'New Policy Created Successfully !');
        } catch (\Exception $exception) {
            return $this->responseError(null, $exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/policy/{user_id}",
     *     tags={"Policy"},
     *     summary="Show User Policy",
     *     description="Show Policy Details",
     *     operationId="PolicyShow",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="user_id", description="show policies of specific user, eg; 1", required=true, in="path", @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="Show Policy list of specific user"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function show(Request $request, $user_id): JsonResponse
    {
        try {
            if ($request->user()->cannot('view', Policy::class))
                return $this->responseError(null, 'Show Policy Is Forbidden', Response::HTTP_FORBIDDEN);

            $data = $this->policyRepository->getByUserID($user_id);
            if (is_null($data)) {
                return $this->responseError(null, 'Policy Not Found', Response::HTTP_NOT_FOUND);
            }

            return $this->responseSuccess($data, 'Policy Details Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * @OA\PUT(
     *     path="/api/policy/{user_id}",
     *     tags={"Policy"},
     *     summary="Update Policy",
     *     description="Update Policy",
     *     @OA\Parameter(name="user_id", description="show policies of specific user, eg; 1", required=true, in="path", @OA\Schema(type="integer")),
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="name", type="string", example="name of Policy"),
     *              @OA\Property(property="status", type="boolean", example=1)
     *          ),
     *      ),
     *     operationId="PolicyUpdate",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200, description="Update Policy"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function update(UpdatePolicyRequest $request, $user_id): JsonResponse
    {
        try {
            if ($request->user()->cannot('update', $this->policyRepository->getByNameAndUserID($user_id,$request->name)))
                return $this->responseError(null, 'Update Policy Is Forbidden', Response::HTTP_FORBIDDEN);

            $data = $this->policyRepository->update($user_id, $request->all());
            if (is_null($data))
                return $this->responseError(null, 'Policy Not Found', Response::HTTP_NOT_FOUND);

            return $this->responseSuccess($data, 'Policy Updated Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\DELETE(
     *     path="/api/policy/{user_id}",
     *     tags={"Policy"},
     *     summary="Delete Policy",
     *     description="Delete Policy",
     *     operationId="PolicyDestroy",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="user_id", description="show policies of specific user, eg; 1", required=true, in="path", @OA\Schema(type="integer")),
     *          *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="name", type="string", example="name of Policy")
     *          ),
     *      ),
     *     @OA\Response(response=200, description="Delete Policy"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function destroy(DestroyPolicyRequest $request, $user_id): JsonResponse
    {
        try {
            if ($request->user()->cannot('delete', $this->policyRepository->getByNameAndUserID($user_id,$request->name)))
                return $this->responseError(null, 'Delete Policy Is Forbidden', Response::HTTP_FORBIDDEN);

            $policy =  $this->policyRepository->getByNameAndUserID($user_id, $request->name);
            if (empty($policy)) {
                return $this->responseError(null, 'Policy Not Found', Response::HTTP_NOT_FOUND);
            }
            $deleted = $this->policyRepository->delete($policy->id);
            if (!$deleted) {
                return $this->responseError(null, 'Failed to delete the Policy.', Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return $this->responseSuccess($policy, 'Policy Deleted Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


}

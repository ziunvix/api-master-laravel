<?php

namespace App\Http\Requests\Policy;

use App\Http\Requests\FormRequest;
use App\Models\User;
use Illuminate\Validation\Rule;
use App\Http\Requests\Policy\MessagePolicyRequest;

class ShowPolicyRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules(): array
    {
        return [
            'perPage' => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}

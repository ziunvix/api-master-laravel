<?php

namespace App\Http\Requests\Policy;


use App\Models\User;

class MessagePolicyRequest
{

    public function messages($user_id, $name): User|array
    {
        $user_full_name = User::find($user_id)->name ?? $user_id;
        return [
            'user_id.unique' => __('validation.custom.messages.policy.user_id_unique', ['USER' => $user_full_name,'NAME' => $name])
        ];
    }
}

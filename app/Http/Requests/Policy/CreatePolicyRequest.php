<?php

namespace App\Http\Requests\Policy;

use App\Http\Requests\FormRequest;
use App\Models\User;
use Illuminate\Validation\Rule;
use App\Http\Requests\Policy\MessagePolicyRequest;

class CreatePolicyRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules(): array
    {

        $user_request = collect([
            'required',
            Rule::unique('policies')->where(function ($query)  {
                $query->where('name', $this->name)
                    ->where('user_id', $this->user_id);
            })
        ]);

        $rule = collect([
            'name' => 'required|max:255',
            'user_id' => $user_request->all(),
            'status' => 'required|boolean'
        ]);

        return $rule->all();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     * Custom validation message
     */
    public function messages()
    {
        $message = new MessagePolicyRequest;

        return $message->messages($this->user_id, $this->name);
    }
}

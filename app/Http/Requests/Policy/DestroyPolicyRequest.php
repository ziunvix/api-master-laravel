<?php

namespace App\Http\Requests\Policy;

use App\Http\Requests\FormRequest;
use App\Http\Requests\Policy\MessagePolicyRequest;
use Illuminate\Validation\Rule;

class DestroyPolicyRequest extends FormRequest
{

    protected function prepareForValidation()
    {
        $this->merge(['user_id' => $this->route()->parameter('policy')]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules(): array
    {

        $rule = collect([
            'name' => 'required|max:255',
            'user_id' => 'required'
        ]);

        return $rule->all();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     * Custom validation message
     */
    public function messages()
    {
        $message = new MessagePolicyRequest;

        return $message->messages($this->user_id, $this->name);
    }
}

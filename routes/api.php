<?php

use App\Http\Controllers\Auth\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Traits\ResponseTrait;
use App\Http\Controllers\ResponseController;
use App\Http\Controllers\Policy\PolicyController;
use App\Http\Controllers\LocaleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'middleware' => 'api'
], function ($router) {

    /**
     * Authentication Module
     */
    Route::group(['prefix' => 'auth'], function() {
        Route::post('register', [AuthController::class, 'register']);
        Route::post('login', [AuthController::class, 'login']);
        Route::post('logout', [AuthController::class, 'logout']);
        Route::post('refresh', [AuthController::class, 'refresh']);
        Route::get('me', [AuthController::class, 'me']);
    });

    /**
     * Policy Module
     */
    Route::resource('policy', PolicyController::class)->whereNumber('policy');
    Route::get('policy/view/all', [PolicyController::class, 'indexAll']);
    Route::get('policy/view/search', [PolicyController::class, 'search']);


    /**
     * local Module
     */
    Route::get('/lang/{locale}', [LocaleController::class, 'set_locale'])->middleware('auth:api')->whereAlpha('locale');

    /**
     * 404 Module
     */
    Route::any('{any}', [ResponseController::class,'not_found' ])->where('any', '.*');

});
